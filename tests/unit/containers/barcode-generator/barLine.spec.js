import { shallowMount } from '@vue/test-utils';

import BarLine from '@/containers/barcode-generator/BarLine';

describe('BarLine', () => {
    describe('should render correct classList', () => {
        it('when number = 0', () => {
            const number = 0;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-1',
                'bar-line--height-1',
            ]);
        });

        it('when number = 1', () => {
            const number = 1;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-1',
                'bar-line--height-2',
            ]);
        });

        it('when number = 2', () => {
            const number = 2;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-1',
                'bar-line--height-3',
            ]);
        });

        it('when number = 3', () => {
            const number = 3;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-1',
                'bar-line--height-4',
            ]);
        });

        it('when number = 4', () => {
            const number = 4;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-2',
                'bar-line--height-1',
            ]);
        });

        it('when number = 5', () => {
            const number = 5;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-2',
                'bar-line--height-2',
            ]);
        });

        it('when number = 6', () => {
            const number = 6;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-2',
                'bar-line--height-3',
            ]);
        });

        it('when number = 7', () => {
            const number = 7;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-2',
                'bar-line--height-4',
            ]);
        });

        it('when number = 8', () => {
            const number = 8;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-3',
                'bar-line--height-1',
            ]);
        });

        it('when number = 9', () => {
            const number = 9;
            const wrapper = shallowMount(BarLine, {
                propsData: { number },
            });
            expect(wrapper.vm.classList).toEqual([
                'bar-line--width-3',
                'bar-line--height-2',
            ]);
        });
    });
});
