import BarcodeService from '@/services/barcode-service';

describe('Barcode Service', () => {
    describe('calculateChecksum', () => {
        it('should calculate correct checksum', () => {
            const bars1 = [8, 5, 3, 3, 2, 1, 8, 1, 9, 2, 1, 6, 2, 3, 0, 1, 3, 6, 7];
            expect(BarcodeService.calculateChecksum(bars1)).toBe(3);

            const bars2 = [5, 4, 8, 9, 8, 5, 0, 3, 5, 4];
            expect(BarcodeService.calculateChecksum(bars2)).toBe(7);

            expect(BarcodeService.calculateChecksum([0, 0])).toBe(0);
            expect(BarcodeService.calculateChecksum([0, 1])).toBe(9);
            expect(BarcodeService.calculateChecksum([0, 2])).toBe(8);
            expect(BarcodeService.calculateChecksum([0, 3])).toBe(7);
            expect(BarcodeService.calculateChecksum([0, 4])).toBe(6);
            expect(BarcodeService.calculateChecksum([0, 5])).toBe(5);
            expect(BarcodeService.calculateChecksum([0, 6])).toBe(4);
            expect(BarcodeService.calculateChecksum([0, 7])).toBe(3);
            expect(BarcodeService.calculateChecksum([0, 8])).toBe(2);
            expect(BarcodeService.calculateChecksum([0, 9])).toBe(1);

            expect(BarcodeService.calculateChecksum([1, 0])).toBe(7);
            expect(BarcodeService.calculateChecksum([2, 0])).toBe(4);
            expect(BarcodeService.calculateChecksum([3, 0])).toBe(1);
        });
    });
});
