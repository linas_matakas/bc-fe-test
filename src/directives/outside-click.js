export default {
    bind(el, binding) {
        setTimeout(() => {
            {
                const handler = (event) => {
                    if ((!el.contains(event.target) && el !== event.target)) {
                        binding.value(event);
                    }
                };
                el.vueClickOutside = handler; // eslint-disable-line no-param-reassign
                document.addEventListener('mousedown', handler);
            }
        }, 0);
    },
    unbind(el) {
        document.removeEventListener('mousedown', el.vueClickOutside);
        el.vueClickOutside = null; // eslint-disable-line no-param-reassign
    },
};
