import Vue from 'vue';
import Input from './input/Input';
import Button from './button/Button';
import ColorPicker from './color-picker/ColorPicker';
import './layout-components';

Vue.component('gc-input', Input);
Vue.component('gc-button', Button);
Vue.component('gc-color-picker', ColorPicker);
