import Vue from 'vue';
import PageFrame from './page-frame/PageFrame';
import Card from './card/Card';

Vue.component('lc-page-frame', PageFrame);
Vue.component('lc-card', Card);
