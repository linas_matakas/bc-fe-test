const calculateChecksum = (barcode) => {
    let odds = 0;
    let evens = 0;
    barcode.forEach((bar, index) => {
        if (index % 2 === 0) {
            odds += bar;
        } else {
            evens += bar;
        }
    });

    const reversedChecksum = ((odds * 3) + evens) % 10;
    return reversedChecksum === 0 ? 0 : 10 - reversedChecksum;
};

export default {
    calculateChecksum,
};
